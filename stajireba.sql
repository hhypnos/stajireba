-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2019 at 07:14 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stajireba`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_statuses`
--

CREATE TABLE `activation_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activationID` int(11) NOT NULL,
  `activation_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `image_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 4, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryID` int(11) NOT NULL,
  `cityID` int(11) NOT NULL,
  `city` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `countryID`, `cityID`, `city`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Tbilisi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryID` int(11) NOT NULL,
  `country` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `countryID`, `country`, `created_at`, `updated_at`) VALUES
(1, 1, 'Georgia', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_questions`
--

CREATE TABLE `form_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `request_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form_questions`
--

INSERT INTO `form_questions` (`id`, `request_name`, `question`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '0', 'რისთვის ეძებთ სტაჟიორს', 1, NULL, NULL),
(2, '1', 'სტაჟირების ხანგძლიობა', 2, NULL, NULL),
(3, '2', 'არის თუ არა შემდგომი დასაქმების პერსპექტივა', 3, NULL, NULL),
(4, '3', 'მოთხოვნები კანდიდატის მიმართ', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `image_thumbnail`, `alt`, `created_at`, `updated_at`) VALUES
(1, 'assets/img/microsoft-home.png', 'assets/img/microsoft-home.png', 'microsoft', NULL, NULL),
(2, 'assets/img/img-home.png', 'assets/img/img-home.png', 'home', NULL, NULL),
(3, 'assets/img/paypal-home.png', 'assets/img/paypal-home.png', 'paypal', NULL, NULL),
(4, 'assets/img/yahoo-home.png', 'assets/img/yahoo-home.png', 'yahoo', NULL, NULL),
(5, 'assets/images/logo.png', 'assets/images/logo.png', 'Stajireba', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_from` double(8,2) DEFAULT NULL,
  `salary_to` double(8,2) DEFAULT NULL,
  `job_type_id` int(11) DEFAULT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countryID` int(11) DEFAULT NULL,
  `cityID` int(11) DEFAULT NULL,
  `zip_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `process_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `company_name`, `contact_email`, `contact_phone`, `salary_from`, `salary_to`, `job_type_id`, `industry`, `role_category`, `countryID`, `cityID`, `zip_code`, `address`, `process_id`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'Future Laboratory', 'test@gmail.com', '+995579800129', 200.00, 500.00, 1, 'IT / SOFTWARE DEV', 'SENIOR DEVELOPER', 1, 1, '0171', '77a kostava', 3, NULL, NULL),
(2, 'test2', 'test2', NULL, 'test@gmail.com', '+995579800129', 200.00, 500.00, 1, 'IT / SOFTWARE DEV', 'SENIOR DEVELOPER', 1, 1, '0171', '77a kostava', 3, NULL, NULL),
(4, 'Burke and Berg Traders', NULL, 'Burke and Berg Traders', 'dybaguro@mailinator.net', '+1 (856) 992-3945', 47.00, 58.00, 1, 'Quo adipisicing dese', 'ჯუნიორ დეველოპერი', 1, 1, NULL, 'Voluptates et quae v', 3, '2019-05-13 00:31:08', '2019-05-13 00:31:08'),
(5, 'Id est delectus con', NULL, 'Boyle and Swanson Inc', 'qosajo@mailinator.com', '+1 (764) 891-6658', 37.00, 25.00, 1, 'Est sequi id quo nos', 'Et rerum ipsa sed r', 1, 1, NULL, 'Quidem saepe sit es', 3, '2019-05-13 01:17:51', '2019-05-13 07:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `job_categories`
--

CREATE TABLE `job_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_category_lists`
--

CREATE TABLE `job_category_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_category_lists`
--

INSERT INTO `job_category_lists` (`id`, `job_id`, `category_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_images`
--

CREATE TABLE `job_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `logoOrCover` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_images`
--

INSERT INTO `job_images` (`id`, `job_id`, `image_id`, `logoOrCover`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_requirements`
--

CREATE TABLE `job_requirements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `requirement_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_requirements`
--

INSERT INTO `job_requirements` (`id`, `job_id`, `requirement_title`, `requirement`, `created_at`, `updated_at`) VALUES
(1, 1, 'Job Responsibilities', 'Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit, Sed Do Eiusmod Tempor Incididunt Ut Labore Et Dolore Magna Aliqua. Ut Enim Ad Minim Veniam, Quis Nostrud Exercitation Ullamco Laboris Nisi Ut Aliquip Ex Ea Commodo Consequat.\r\n\r\nLorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit, Sed Do Eiusmod Tempor Incididunt Ut Labore Et Dolore Magna Aliqua. Ut Enim Ad Minim Veniam, Quis Nostrud Exercitation Ullamco Laboris Nisi Ut Aliquip Ex Ea Commodo Consequat.', NULL, NULL),
(2, 1, 'Job Skills', 'Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit, Sed Do Eiusmod Tempor Incididunt Ut Labore Et Dolore Magna Aliqua. Ut Enim Ad Minim Veniam, Quis Nostrud Exercitation Ullamco Laboris Nisi Ut Aliquip Ex Ea Commodo Consequat.\r\n\r\nLorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit, Sed Do Eiusmod Tempor Incididunt Ut Labore Et Dolore Magna Aliqua. Ut Enim Ad Minim Veniam, Quis Nostrud Exercitation Ullamco Laboris Nisi Ut Aliquip Ex Ea Commodo Consequat.', NULL, NULL),
(3, 4, 'მიმართვა (რისთვის ეძებთ სტაჟიორს)', 'მიმართვა', '2019-05-13 00:31:08', '2019-05-13 00:31:08'),
(4, 4, 'სტაჟირების ხანგძლიობა', 'ხანგძლიობა', '2019-05-13 00:31:08', '2019-05-13 00:31:08'),
(5, 4, 'არის თუ არა შემდგომი დასაქმების პერსპექტივა', 'დასაქმების პერსპექტივა', '2019-05-13 00:31:08', '2019-05-13 00:31:08'),
(6, 4, 'მოთხოვნები კანდიდატის მიმართ', 'კანდიდატის მიმართ', '2019-05-13 00:31:08', '2019-05-13 00:31:08'),
(7, 5, 'მიმართვა (რისთვის ეძებთ სტაჟიორს)', 'Eveniet at est cumq', '2019-05-13 01:17:51', '2019-05-13 01:17:51'),
(8, 5, 'სტაჟირების ხანგძლიობა', 'Odit sit quas culpa', '2019-05-13 01:17:51', '2019-05-13 01:17:51'),
(9, 5, 'არის თუ არა შემდგომი დასაქმების პერსპექტივა', 'Eum qui non providen', '2019-05-13 01:17:51', '2019-05-13 01:17:51'),
(10, 5, 'მოთხოვნები კანდიდატის მიმართ', 'Tenetur sit pariatur', '2019-05-13 01:17:51', '2019-05-13 01:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE `job_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_type` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`id`, `job_type`, `color`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Full Time', '#11b719', NULL, NULL, NULL),
(2, 'Part Time', '#ff9800', NULL, NULL, NULL),
(3, 'Freelancer', '#1194f7', NULL, NULL, NULL),
(4, 'Enternship', '#f21136', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `langID` int(11) NOT NULL,
  `language` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langAcronym` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langImage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_08_08_004003_create_languages_table', 1),
(2, '2018_11_08_123829_create_translates_table', 1),
(3, '2018_11_15_203433_create_users_table', 1),
(4, '2018_11_25_232604_create_static_words_table', 1),
(5, '2018_11_29_000011_create_static_images_table', 1),
(6, '2019_04_20_233107_create_images_table', 1),
(7, '2019_04_20_233347_create_user_images_table', 1),
(8, '2019_04_20_233605_create_user_settings_table', 1),
(9, '2019_04_23_212531_create_activation_statuses_table', 1),
(11, '2019_05_11_230149_create_brands_table', 2),
(12, '2018_07_15_160724_create_countries_table', 3),
(13, '2018_08_09_090219_create_cities_table', 3),
(14, '2019_05_12_083302_create_jobs_table', 4),
(15, '2019_05_12_085254_create_job_requirements_table', 4),
(16, '2019_05_12_085434_create_job_categories_table', 4),
(17, '2019_05_12_090006_create_job_category_lists_table', 4),
(18, '2019_05_12_090215_create_job_images_table', 5),
(19, '2019_05_12_091053_create_job_types_table', 6),
(20, '2019_05_12_222728_create_form_questions_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `static_images`
--

CREATE TABLE `static_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` int(11) NOT NULL,
  `page` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_images`
--

INSERT INTO `static_images` (`id`, `key`, `page`, `url`, `image`, `alt`, `created_at`, `updated_at`) VALUES
(1, 1, 'all', '/', 'assets/images/logo.png', NULL, NULL, NULL),
(2, 1, 'home', '#', 'assets/img/bn2.jpg', NULL, NULL, NULL),
(3, 1, 'jobs', '#', 'assets/img/banner-10.jpg', NULL, NULL, NULL),
(4, 1, 'contacts', '#', 'assets/img/banner-10.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `static_words`
--

CREATE TABLE `static_words` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` int(11) NOT NULL,
  `page` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `word` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_words`
--

INSERT INTO `static_words` (`id`, `key`, `page`, `url`, `word`, `created_at`, `updated_at`) VALUES
(1, 1, 'home', '#', '7,000+ Browse Jobs', NULL, NULL),
(2, 1, 'jobs', '#', 'მიიღე პრაქტიკული', NULL, NULL),
(3, 1, 'contacts', '#', 'გამოაქვეყნე ვაკანსია', NULL, NULL),
(4, 2, 'contacts', '#', 'გამოაქვეყნეთ თქვენი განცხადება ჩვენს საიტზე და მოიზიდეთ თქვენს კომპანიაში სტაჟირების გასავლელად საუკეთესო კადრები.\nმეილზე გამოგვიგზავნეთ სტაჟირების პირობები ან შეავსეთ ფორმა:', NULL, NULL),
(5, 3, 'contacts', '#', '77a, M. Kostava str. Tbilisi, 0171, Georgia', NULL, NULL),
(6, 4, 'contacts', '#', 'info@futurelab.ge', NULL, NULL),
(7, 5, 'contacts', '#', 'ფორმის შევსება', NULL, NULL),
(8, 2, 'jobs', '#', 'გამოცდილება', NULL, NULL),
(9, 1, 'jobdetails', '#', 'დეტალურად', NULL, NULL),
(10, 2, 'jobdetails', '#', 'Apply This Job', NULL, NULL),
(11, 1, 'aboutus', '#', 'ჩვენს შესახებ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `translates`
--

CREATE TABLE `translates` (
  `id` int(10) UNSIGNED NOT NULL,
  `origin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `langID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `email` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `status` enum('offline','online') COLLATE utf8mb4_unicode_ci NOT NULL,
  `loginTime` datetime DEFAULT NULL,
  `logoutTime` datetime DEFAULT NULL,
  `activation` int(11) NOT NULL DEFAULT '1',
  `user_or_company` int(11) NOT NULL,
  `verifyied_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `gender_id`, `email`, `password`, `username`, `dateOfBirth`, `status`, `loginTime`, `logoutTime`, `activation`, `user_or_company`, `verifyied_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 'admin@gmail.com', '$2y$12$n5gBycdxguPCxZiSuCNkZeTt2ubTI1YcPTOuSNBnalAKRKo4uAI3K', NULL, NULL, 'online', '2019-05-13 06:50:36', NULL, 1, 1, '2019-05-13 00:00:00', 'fZelXvsLpPQ9NJMkxdRF5g4x7N9v5KbtDEAjBzLaikSoqdwLvMQqrqN4258L', NULL, '2019-05-13 06:50:36');

-- --------------------------------------------------------

--
-- Table structure for table `user_images`
--

CREATE TABLE `user_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `defaultOrNot` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE `user_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publicity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activation_statuses`
--
ALTER TABLE `activation_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_questions`
--
ALTER TABLE `form_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_categories`
--
ALTER TABLE `job_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_category_lists`
--
ALTER TABLE `job_category_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_images`
--
ALTER TABLE `job_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_requirements`
--
ALTER TABLE `job_requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_types`
--
ALTER TABLE `job_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_images`
--
ALTER TABLE `static_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_words`
--
ALTER TABLE `static_words`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translates`
--
ALTER TABLE `translates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activation_statuses`
--
ALTER TABLE `activation_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_questions`
--
ALTER TABLE `form_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `job_categories`
--
ALTER TABLE `job_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_category_lists`
--
ALTER TABLE `job_category_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `job_images`
--
ALTER TABLE `job_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `job_requirements`
--
ALTER TABLE `job_requirements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `job_types`
--
ALTER TABLE `job_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `static_images`
--
ALTER TABLE `static_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `static_words`
--
ALTER TABLE `static_words`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `translates`
--
ALTER TABLE `translates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
