<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@jobs');
Route::get('jobs', 'PageController@jobs');
Route::get('jobs/{id}', 'PageController@job_details');
Route::get('about-us', 'PageController@about_us');


Route::post('sendform', 'PageController@send_form');
Route::get('infomessage', 'PageController@info_message');
Route::get('contacts', 'PageController@contacts');

Route::group(['middleware' => ['auth']], function () {

    Route::get('createjob', 'AccountController@create_job');
    Route::post('createjob', 'AccountController@add_job');

    Route::get('editjob/{id}', 'AccountController@edit_job');
    Route::post('editjob', 'AccountController@update_job');

    Route::get('jobsbystatus', 'AccountController@jobs_by_status');


    Route::get('logout', 'AccountController@logout')->name('logout');

});

Route::group(['middleware' => ['guest']], function () {

    Route::get('login', 'PageController@login')->name('login');
//    Route::post('registration', 'AccountController@registration');
    Route::post('login', 'AccountController@login');

});