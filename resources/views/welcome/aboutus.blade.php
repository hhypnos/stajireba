@extends('layouts.master')
@section('content')
    <div class="clearfix"></div>

    <section class="inner-header-title" style="background-image:url({{URL::to($static_image('contacts',1)['image'])}});">
        <div class="container">
            <h1>{{$static_word('aboutus',1)['word']}}</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Title Header End -->

    <!-- Contact Page Section Start -->
    <section class="contact-page">
        <div class="container">
            <p>{{$static_word('aboutus',2)['word']}}</p>
        </div>
    </section>
    <!-- contact section End -->
@endsection