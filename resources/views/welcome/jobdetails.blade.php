@extends('layouts.master')
@section('content')
    <!-- Title Header Start -->
    <section class="inner-header-title" style="background-image:url({{URL::to($job['cover']['image']['image'])}});">
        <div class="container">
            <h1>{{$static_word('jobdetails',1)['word']}}</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Title Header End -->

    <!-- Candidate Detail Start -->
    <section class="detail-desc">
        <div class="container">

            <div class="ur-detail-wrap top-lay">

                <div class="ur-detail-box">

                    <div class="ur-thumb">
                        <img src="{{URL::to(check_image($job['logo']['image']['image'],'image'))}}" class="img-responsive" alt="{{URL::to($job['cover']['image']['alt'])}}" />
                    </div>
                    <div class="ur-caption">
                        <h4 class="ur-title">{{$job->title}}</h4>
                        <p class="ur-location"><i class="ti-location-pin mrg-r-5"></i>{{$job->address}}, {{$job->city->city}},{{$job->zip_code}}, {{$job->country->country}}</p>
                        <span class="ur-designation"><i class="ti-home mrg-r-5"></i>{{$job->company_name}}</span>
                    </div>

                </div>

                {{--<div class="ur-detail-btn">--}}
                    {{--<a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn btn-warning mrg-bot-10 full-width"><i class="ti-star mrg-r-5"></i>{{$static_word('jobdetails',2)['word']}}</a><br>--}}
                {{--</div>--}}

            </div>

        </div>
    </section>

    <!-- Job full detail Start -->
    <section class="full-detail-description full-detail">
        <div class="container">
            <!-- Job Description -->
            <div class="col-md-12 col-sm-12">
                <div class="full-card">

                    <div class="row row-bottom mrg-0">
                        <h2 class="detail-title">{{translate('Job Detail',session('languageID'))}}</h2>
                        <ul class="job-detail-des">
                            <li><span>{{translate('Salary',session('languageID'))}}:</span>{{currency($job->salary_from)}} - {{currency($job->salary_to)}}</li>
                            <li><span>{{translate('Industry',session('languageID'))}}:</span>{{$job->industry}}</li>
                            <li><span>{{translate('Role',session('languageID'))}}:</span>{{$job->role_category}}</li>
                            <li><span>{{translate('Job Type',session('languageID'))}}:</span>{{$job->job_type->job_type}}</li>
                        </ul>
                    </div>

                    <div class="row row-bottom mrg-0">
                        <h2 class="detail-title">{{translate('Location',session('languageID'))}}</h2>
                        <ul class="job-detail-des">
                            <li><span>{{translate('Address',session('languageID'))}}:</span>{{$job->address}}</li>
                            <li><span>{{translate('City',session('languageID'))}}:</span>{{$job->city->city}}</li>
                            <li><span>{{translate('Country',session('languageID'))}}:</span>{{$job->country->country}}</li>
                            <li><span>{{translate('Zip',session('languageID'))}}:</span>{{$job->zip_code}}</li>
                            <li><span>{{translate('Telephone',session('languageID'))}}:</span>{{$job->contact_phone}}</li>
                            <li><span>{{translate('Email',session('languageID'))}}:</span>{{$job->contact_email}}</li>
                        </ul>
                    </div>

                    @foreach($job->job_requirements as $requirement)
                        <div class="row row-bottom mrg-0">
                            <h2 class="detail-title">{{$requirement->requirement_title}}</h2>
                            {{$requirement->requirement}}
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End Job Description -->
        </div>
    </section>
    <!-- Job full detail End -->
    {{--<!-- Apply Form Code -->--}}
    {{--<div class="modal fade" id="apply-job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-body">--}}
                    {{--<div class="apply-job-box">--}}
                        {{--<img src="assets/img/com-1.jpg" class="img-responsive" alt="">--}}
                        {{--<h4>{{$job->title}}</h4>--}}
                        {{--<p>{{$job->company_name}}</p>--}}
                    {{--</div>--}}
                    {{--<div class="apply-job-form">--}}
                        {{--<form class="form-inline" method="post">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" name="name" class="form-control" placeholder="Your Name" required="">--}}
                                    {{--<input type="email" name="email" class="form-control" placeholder="Your Email" required="">--}}
                                    {{--<div class="fileUpload">--}}
                                        {{--<span>Upload CV</span>--}}
                                        {{--<input type="file" class="upload" />--}}
                                    {{--</div>--}}
                                    {{--<div class="center">--}}
                                        {{--<button type="submit" id="subscribe" class="submit-btn"> Apply Now </button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- End Apply Form -->--}}
@endsection