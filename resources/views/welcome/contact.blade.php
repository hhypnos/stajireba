@extends('layouts.master')
@section('content')
    <div class="clearfix"></div>

    <section class="inner-header-title" style="background-image:url({{URL::to($static_image('contacts',1)['image'])}});">
        <div class="container">
            <h1>{{$static_word('contacts',1)['word']}}</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Title Header End -->

    <!-- Contact Page Section Start -->
    <section class="contact-page">
        <div class="container">
            <h2>{{$static_word('contacts',2)['word']}}</h2>

            <div class="col-md-4 col-sm-4">
                <div class="contact-box">
                    <i class="fa fa-envelope"></i>
                    <p style="text-transform: none">{{$static_word('contacts',4)['word']}}</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="contact-box">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job">
                        <i class="fa fa-file-text"></i>
                        <p>{{$static_word('contacts',5)['word']}}</p>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="contact-box">
                    <i class="fa fa-map-marker"></i>
                    <p>{{$static_word('contacts',3)['word']}}</p>
                </div>
            </div>

        </div>
    </section>
    <!-- contact section End -->


    <!-- Apply Form Code -->
    <div class="modal fade" id="apply-job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="apply-job-box">
                        <img src="{{URl::to($static_image('all',1)['image'])}}" class="img-responsive" alt="{{URl::to($static_image('all',1)['alt'])}}" style="max-width: 250px !important;">
                        <h4>შეავსეთ ვაკანსიის ფორმა</h4>
                    </div>
                    <div class="apply-job-form">
                        <form class="form-inline" method="post" action="{{URL::to('sendform')}}">
                            {{csrf_field()}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" name="job_title" class="form-control" value="{{old('job_title')}}" placeholder="{{translate('ვაკანსიის დასახელება',session('languageID'))}}" required="">
                                    <input type="text" name="company_name" class="form-control" value="{{old('company_name')}}" placeholder="{{translate('კომპანიის სახელი',session('languageID'))}}" required="">
                                    <input type="text" name="industry" class="form-control" value="{{old('industry')}}" placeholder="{{translate('სტაჟირების მიმართულება',session('languageID'))}}" required="">
                                    <input type="text" name="role_category" class="form-control" value="{{old('role_category')}}" placeholder="{{translate('პოზიცია',session('languageID'))}}" required="">
                                    <input type="number" name="salary_from" class="form-control" min="0" value="{{old('salary_from')}}" placeholder="{{translate('თუ მოიცავს ანაზღაურებას მიუთითეთ თანხის საწყისი ოდენობა',session('languageID'))}}" required="">
                                    <input type="number" name="salary_to" class="form-control" min="0" value="{{old('salary_to')}}" placeholder="{{translate('თუ მოიცავს ანაზღაურებას მიუთითეთ თანხის მაქსიმალური ოდენობა',session('languageID'))}}" required="">
                                    @foreach($form_questions as $form_question)
                                    <textarea type="text" name="requirement[]" class="form-control" placeholder="{{translate($form_question->question,session('languageID'))}}" required="">{{old('requirement[]')}}</textarea>
                                    @endforeach
                                    <select name="job_type" class="form-control">
                                        <option value="empty" disabled selected>{{translate('აირჩიეთ სტაჟირების გრაფიკი',session('languageID'))}}</option>
                                        @foreach($job_types as $job_type)
                                        <option value="{{$job_type->id}}">{{$job_type->job_type}}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="contact_email" class="form-control"  value="{{old('contact_email')}}" placeholder="{{translate('მეილი',session('languageID'))}}">
                                    <input type="text" name="contact_phone" class="form-control"  value="{{old('contact_phone')}}" placeholder="{{translate('ტელეფონი',session('languageID'))}}">
                                    <select name="city" class="form-control">
                                        <option value="empty" disabled selected>{{translate('აირჩიეთ ქალაქი',session('languageID'))}}</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->cityID}}">{{$city->city}}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="address" class="form-control"  value="{{old('address')}}" placeholder="{{translate('მისამართი',session('languageID'))}}">
                                    <input type="text" name="zip_code" class="form-control"  value="{{old('zip_code')}}" placeholder="{{translate('ფოსტის კოდი',session('languageID'))}}">
                                    <div class="center">
                                        <button type="submit" id="subscribe" class="submit-btn">{{translate('გაგზავნა',session('languageID'))}}</button>
                                    </div>
                                </div>
                            </div>
                            @include('layouts.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Apply Form -->
@endsection