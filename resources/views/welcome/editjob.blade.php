@extends('layouts.master')
@section('content')
    <!-- Header Title Start -->
    <section class="inner-header-title blank">
        <div class="container">
            <h1>{{translate('ვაკანსიის დამატება',session('languageID'))}}</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Header Title End -->

    <!-- General Detail Start -->
    <div class="detail-desc section">
        <div class="container white-shadow">

            <div class="row">
                <div class="detail-pic js">
                    <div class="box">
                        <input type="file" name="upload-pic[]" id="upload-pic" class="inputfile" />
                        <label for="upload-pic"><i class="fa fa-upload" aria-hidden="true"></i><span></span></label>
                    </div>
                </div>
            </div>

            <div class="row bottom-mrg">
                <form class="add-feild" method="post" action="{{URL::to('editjob')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="job_id" value="{{$job->id}}">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" name="job_title" class="form-control" value="{{$job->title}}" placeholder="{{translate('ვაკანსიის დასახელება',session('languageID'))}}" required="">
                            <input type="text" name="company_name" class="form-control" value="{{$job->company_name}}" placeholder="{{translate('კომპანიის სახელი',session('languageID'))}}" required="">
                            <input type="text" name="industry" class="form-control" value="{{$job->industry}}" placeholder="{{translate('სტაჟირების მიმართულება',session('languageID'))}}" required="">
                            <input type="text" name="role_category" class="form-control" value="{{$job->role_category}}" placeholder="{{translate('პოზიცია',session('languageID'))}}" required="">
                            <input type="number" name="salary_from" class="form-control" min="0" value="{{$job->salary_from}}" placeholder="{{translate('თუ მოიცავს ანაზღაურებას მიუთითეთ თანხის საწყისი ოდენობა',session('languageID'))}}" required="">
                            <input type="number" name="salary_to" class="form-control" min="0" value="{{$job->salary_to}}" placeholder="{{translate('თუ მოიცავს ანაზღაურებას მიუთითეთ თანხის მაქსიმალური ოდენობა',session('languageID'))}}" required="">
                            @foreach($job->job_requirements as $requirement)
                                {{$requirement->requirement_title}}
                                <textarea type="text" name="requirement[]" class="form-control" placeholder="{{translate($requirement->question,session('languageID'))}}" required="">{{translate($requirement->requirement,session('languageID'))}}</textarea>
                            @endforeach
                            <select name="job_type" class="form-control">
                                <option value="empty" disabled selected>{{translate('აირჩიეთ სტაჟირების გრაფიკი',session('languageID'))}}</option>
                                @foreach($job_types as $job_type)
                                    <option value="{{$job_type->id}}" @if($job->job_type_id == $job_type->id) selected @endif>{{$job_type->job_type}}</option>
                                @endforeach
                            </select>
                            <input type="text" name="contact_email" class="form-control"  value="{{$job->contact_email}}" placeholder="{{translate('მეილი',session('languageID'))}}">
                            <input type="text" name="contact_phone" class="form-control"  value="{{$job->contact_phone}}" placeholder="{{translate('ტელეფონი',session('languageID'))}}">
                            <select name="city" class="form-control">
                                <option value="empty" disabled selected>{{translate('აირჩიეთ ქალაქი',session('languageID'))}}</option>
                                @foreach($cities as $city)
                                    <option value="{{$city->cityID}}" @if($job->cityID == $city->cityID) selected @endif>{{$city->city}}</option>
                                @endforeach
                            </select>
                            <input type="text" name="address" class="form-control"  value="{{$job->address}}" placeholder="{{translate('მისამართი',session('languageID'))}}">
                            <input type="text" name="zip_code" class="form-control"  value="{{$job->zip_code}}" placeholder="{{translate('ფოსტის კოდი',session('languageID'))}}">
                            <select name="process_id" class="form-control">
                                <option value="empty" disabled selected>{{translate('აირჩიეთ სტატუსი',session('languageID'))}}</option>
                                    <option value="1">გამოგზავნილია</option>
                                    <option value="2">უარყოფა</option>
                                    <option value="3">გამოქვეყნება</option>
                            </select>
                            <div class="center modal" style="position: relative;display: block">
                                <button type="submit" class="submit-btn">{{translate('განახლება',session('languageID'))}}</button>
                            </div>
                        </div>
                    </div>
                    @include('layouts.errors')
                </form>
            </div>

        </div>
    </div>
    <!-- General Detail End -->
@endsection