@extends('layouts.master')
@section('content')
    <div class="clearfix"></div>

    <section class="inner-header-title" style="background-image:url({{URL::to($static_image('contacts',1)['image'])}});">
        <div class="container">
            <h1 style="font-size:65px;">{{session('info_message')}}</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Title Header End -->

    <!-- Contact Page Section Start -->
    <section class="contact-page">
        <div class="container">
            <h2>{{session('info_message_text')}}</h2>
        </div>
    </section>
    <!-- contact section End -->

@endsection