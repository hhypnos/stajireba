@extends('layouts.master')
@section('content')
    <div class="banner" style="background-image:url({{URL::to($static_image('home',1)['image'])}});">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>{{$static_word('home',1)['word']}}</h1>
                    <form class="form-horizontal">
                        {{--<div class="col-md-10 no-padd">--}}
                            {{--<div class="input-group">--}}
                                {{--<select id="choose-city" class="form-control">--}}
                                    {{--<option>Choose City</option>--}}
                                    {{--<option>Chandigarh</option>--}}
                                    {{--<option>London</option>--}}
                                    {{--<option>England</option>--}}
                                    {{--<option>Pratapcity</option>--}}
                                    {{--<option>Ukrain</option>--}}
                                    {{--<option>Wilangana</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-2 no-padd">--}}
                            {{--<div class="input-group">--}}
                                {{--<button type="submit" class="btn btn-primary">Search Job</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </form>
                </div>
            </div>
        </div>
        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    @foreach($brands as $brand)
                        <div class="brand-img"><img src="{{$brand->image->image}}" class="img-responsive" alt="{{$brand->image['alt']}}" /></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


@endsection