@extends('layouts.master')
@section('content')
    <div class="clearfix"></div>

    <!-- Title Header Start -->
    <section class="inner-header-title no-br advance-header-title" style="background-image:url({{URL::to($static_image('jobs',1)['image'])}});">
        <div class="container">
            <h2><span>{{$static_word('jobs',1)['word']}}</span> {{$static_word('jobs',2)['word']}}</h2>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Title Header End -->

    <!-- bottom form section start -->
    <section class="bottom-search-form">
        {{--<div class="container">--}}
            {{--<form class="bt-form">--}}
                {{--<div class="col-md-9 col-sm-6">--}}
                    {{--<select class="form-control">--}}
                        {{--<option>By Category</option>--}}
                        {{--<option>Information Technology</option>--}}
                        {{--<option>Mechanical</option>--}}
                        {{--<option>Hardware</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<button type="submit" class="btn btn-primary">Search Job</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    </section>
    <!-- Bottom Search Form Section End -->

    <!-- ========== Begin: Brows job Category ===============  -->
    <section class="brows-job-category gray-bg">
        <div class="container">
            <div class="col-md-12 col-sm-12">
                <div class="full-card">

                    <div class="card-header">
                        <div class="row mrg-0">
                            <div class="col-md-12 col-sm-4">
                                <ol class="breadcrumb pull-right">
                                    <li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
                                    <li class="active">{{translate('ვაკანსიები',session('languageID'))}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @foreach($jobs as $job)
                        <article class="advance-search-job">
                            <div class="brows-job-list">
                                <div class="col-md-6 col-sm-6">
                                    <div class="item-fl-box">
                                        <div class="brows-job-company-img">
                                            <a href="{{URL::to('jobs/'.$job->id)}}"><img src="{{URL::to(check_image($job->logo['image']['image'],'image'))}}" class="img-responsive" alt="{{$job->logo['image']['alt']}}" /></a>
                                        </div>
                                        <div class="brows-job-position">
                                            <h3><a href="{{URL::to('jobs/'.$job->id)}}">{{$job->title}}</a></h3>
                                            <p>
                                                <span class="brows-job-sallery"><i class="fa fa-money"></i>{{currency($job->salary_from)}} - {{currency($job->salary_to)}}</span>
                                                <span class="job-type bg-trans-primary" style="background: {{$job->job_type->color}}!important">{{$job->job_type->job_type}}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-job-location">
                                        <p><i class="fa fa-map-marker"></i>{{$job->address}}, {{$job->city->city}},{{$job->zip_code}}, {{$job->country->country}}</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-job-link">
                                        <br>
                                        <p><i class="fa fa-eye"></i>{{$job->job_views->count()}}</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-job-link">
                                        <a href="{{URL::to('editjob/'.$job->id)}}" class="btn btn-default">{{translate('რედაქტირება',session('languageID'))}}</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                        @endforeach

                    </div>
                </div>

                <div class="row">
                    <ul class="pagination">
                        {{$jobs->links()}}
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- ========== Begin: Brows job Category End ===============  -->
@endsection