<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="canonical" href="{{env('APP_URL')}}" />
        @foreach($languages as $language)
            <link rel="alternate" hreflang="{{$language->langAcronym}}" href="https://georgia4you.ge/language/{{$language->langID}}" />
        @endforeach
        <meta property="og:site_name" content="{{env('APP_NAME')}}" />
        <meta property="og:title" content="{{$title}}" />
        <meta name="og:description" content="{{$description}}" />
        <meta property="og:url" content="{{env('APP_URL')}}" />
        <meta property="og:image" content="#" />
        <meta property="og:type" content="website" />

        {{--<link rel="apple-touch-icon" sizes="180x180" href="#">--}}
        {{--<link rel="icon" type="image/png" sizes="32x32" href="#">--}}
        {{--<link rel="icon" type="image/png" sizes="16x16" href="#">--}}
        <link rel="shortcut icon" href="{{URL::to('assets/images/logo.png')}}">
        <link rel="mask-icon" href="" color="#a3ce54">
        <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
        <meta name="application-name" content="{{env('APP_NAME')}}">
        <meta name="theme-color" content="#a3ce54">
        <meta name="description" content="{{$description}}" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="{{$title}}" />
        <script type="application/ld+json">
        {
          "@context": "https:\/\/schema.org",
          "@type": "WebSite",
          "url": "{{url()->full()}}",
          "name": "{{$title}}",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "",
            "contactType": "Customer service"
          },
          "logo":"{{URL::to('assets/images/fav.ico')}}",
          "sameAs":["https:\/\/www.facebook.com\/InnovationConsultingFirm"]

        }
</script>
        <title>{{$title}}</title>
        <!-- CSS==================================================-->
        <link rel="stylesheet" href="{{URL::to('assets/plugins/css/plugins.css')}}">
        <link href="{{URL::to('assets/css/style.css')}}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" id="jssDefault" href="{{URL::to('assets/css/colors/green-style.css')}}">



</head>
<body>
<div class="Loader"></div>
@include('layouts.navigation')