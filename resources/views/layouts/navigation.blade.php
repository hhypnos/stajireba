 <nav class="navbar navbar-default navbar-fixed navbar-light white bootsnav">

        <div class="container">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{URl::to($static_image('all',1)['url'])}}">
                    <img src="{{URL::to($static_image('all',1)['image'])}}" class="logo logo-scrolled" alt="{{URL::to($static_image('all',1)['alt'])}}">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="{{URL::to('/')}}"><i class="fa fa-home" aria-hidden="true"></i>
                            {{translate('მთავარი',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('jobs')}}"><i class="fa fa-tasks" aria-hidden="true"></i>{{translate('ვაკანსიები',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('contacts')}}"><i class="fa fa-pencil" aria-hidden="true"></i>{{translate('გამოაქვეყნე ვაკანსია',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('about-us')}}"><i class="fa fa-question-circle" aria-hidden="true"></i>
                            {{translate('ჩვენს შესახებ',session('languageID'))}}</a></li>
                    @auth
                        <li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{translate('სამართავი პანელი',session('languageID'))}}</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                                <ul class="menu-col">
                                                    <li><a href="{{URL::to('createjob')}}">ახალი ვაკანსიის დამატება</a></li>
                                                    <li><a href="{{URL::to('jobsbystatus?process_id=1')}}">გამოგზავნილი ვაკანსიები</a></li>
                                                    <li><a href="{{URL::to('jobsbystatus?process_id=3')}}">გამოქვეყნებული ვაკანსიები</a></li>
                                                    <li><a href="{{URL::to('jobsbystatus?process_id=2')}}">უარყოფილი ვაკანსიები</a></li>
                                                    <li><a href="{{URL::to('logout')}}">გამოსვლა</a></li>
                                                </ul>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                    @endauth
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
<div class="clearfix"></div>