<footer class="footer">
    <div class="row lg-menu">
        <div class="container">
            <div class="col-md-4 col-sm-4">
                <img src="{{URL::to($static_image('all',1)['image'])}}" class="img-responsive" alt="{{URL::to($static_image('all',1)['alt'])}}" />
            </div>
            <div class="col-md-8 co-sm-8 pull-right">
                <ul>
                    <li><a href="{{URL::to('/')}}" title="{{translate('მთავარი',session('languageID'))}}">{{translate('მთავარი',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('jobs')}}" title="{{translate('ვაკანსიები',session('languageID'))}}">{{translate('ვაკანსიები',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('contacts')}}" title="{{translate('ჩვენს შესახებ',session('languageID'))}}">{{translate('ჩვენს შესახებ',session('languageID'))}}</a></li>
                    <li><a href="{{URL::to('contacts')}}" title="{{translate('დაგვიკავშირდით',session('languageID'))}}">{{translate('გამოაქვეყნე ვაკანსია',session('languageID'))}}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row no-padding">
        <div class="container">
            <div class="col-md-9 col-sm-12">
                <div class="footer-widget">
                    {{--<h3 class="widgettitle widget-title">ჩვენს შესახებ</h3>--}}
                    <div class="textwidget">
                        {{--<p>გამოაქვეყნეთ თქვენი განცხადება ჩვენს საიტზე და მოიზიდეთ თქვენს კომპანიაში სტაჟირების გასავლელად საუკეთესო კადრები</p>--}}

                        {{--<p>მეილზე გამოგვიგზავნეთ სტაჟირების პირობები:</p>--}}
                        {{--<p><strong>მეილი:</strong> info@futurelab.ge</p>--}}
                        {{--<p><strong>Call:</strong> <a href="tel:+15555555555">555-555-1234</a></p>--}}
                        <ul class="footer-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">ნავიგაცია</h3>
                    <div class="textwidget">
                        <div class="textwidget">
                            <ul class="footer-navigation">
                                <li><a href="{{URL::to('/')}}">{{translate('მთავარი',session('languageID'))}}</a></li>
                                <li><a href="{{URL::to('jobs')}}">{{translate('ვაკანსიები',session('languageID'))}}</a></li>
                                <li><a href="{{URL::to('about-us')}}">{{translate('ჩვენს შესახებ',session('languageID'))}}</a></li>
                                <li><a href="{{URL::to('contacts')}}">{{translate('გამოაქვეყნე ვაკანსია',session('languageID'))}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row copyright">
        <div class="container">
            <p>Stajireba.ge © 2019. ყველა უფლება დაცულია</p>
        </div>
    </div>
</footer>
<!-- Scripts==================================================-->
<script type="text/javascript" src="{{URL::to('assets/plugins/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/viewportchecker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/bootsnav.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/datedropper.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/dropzone.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/loader.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/gmap3.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/js/jquery.easy-autocomplete.min.js')}}"></script>
<script src="{{URL::to('assets/js/custom.js')}}"></script>
<script src="{{URL::to('assets/js/jQuery.style.switcher.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#styleOptions').styleSwitcher();
    });
</script>
<script>
    function openRightMenu() {
        document.getElementById("rightMenu").style.display = "block";
    }

    function closeRightMenu() {
        document.getElementById("rightMenu").style.display = "none";
    }
</script>
</body>
</html>