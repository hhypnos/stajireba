<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Static_word extends Model
{

    public function scopeStatic($query,$page,$key)
    {
            return $query->where('page',$page)->where('key',$key);
    }
}
