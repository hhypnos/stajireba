<?php

/**
* translate static texts
*
* @param $word
* @param $lang
*/
function translate($word, $lang)
{
    if(DB::table('translates')->where('langID', $lang)->where('origin',$word)->first()){
        return DB::table('translates')->where('langID', $lang)->where('origin',$word)->first()->translated;
    }else{
        return $word;
    }
}

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

/**
 * currency
 *
 * @param $number
 */
function currency($number)
{
    if(session('currency')){
        $client = new SoapClient('http://nbg.gov.ge/currency.wsdl');
        if($client->GetCurrency(session('currency.currency')) != ""){
            $bankCur = $client->GetCurrency(session('currency.currency'));
        }else{
            $bankCur = 1 ;
        }
        return round($number/$bankCur,2)."".session('currency.logo');
    }else{
        return $number."₾";
    }
}

/**
 * Get room cover
 *
 * @param $column
 * @param $alt
 */
function check_image($column = null ,$param = null){
    switch ($param){
        case 'image':
            return is_null($column) ? 'assets/images/logo.png' : $column;
        case 'alt':
            return is_null($column) ? 'noalt' : $column;
        case 'thumbnail':
            return is_null($column) ? 'assets/images/logo.png' : $column;
        default:
            return '';
    }

}