<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.*','welcome.*'],  function(View $view){
            $view->with('languages', \App\Language::get());

            $view->with('static_word', function($page,$key){
                return translate(\App\Static_word::where('page',$page)->where('key',$key)->first(),
                session("languageID"));
            });

            $view->with('static_image', function($page,$key){
                return \App\Static_image::where('page',$page)->where('key',$key)->first();
            });

        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
