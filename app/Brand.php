<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function image(){
        return $this->belongsTo('App\Image','image_id','id');
    }
}
