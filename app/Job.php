<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function job_type(){
        return $this->hasOne('App\Job_type','id','job_type_id');
    }

    public function country(){
        return $this->hasOne('App\Country','countryID','countryID');
    }

    public function city(){
        return $this->hasOne('App\City','cityID','cityID');
    }

    public function logo(){
        return $this->hasOne('App\Job_image','job_id','id')->where('logoOrCover',1);
    }

    public function cover(){
        return $this->hasOne('App\Job_image','job_id','id')->where('logoOrCover',2);
    }

    public function job_requirements(){
        return $this->hasMany('App\Job_requirement','job_id','id');
    }

    public function job_views(){
        return $this->hasMany('App\Job_view','job_id','id');
    }
}
