<?php

namespace App\Http\Controllers;

use App\City;
use App\Form_question;
use App\Job;
use App\Job_requirement;
use App\Job_type;
use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function login(){
        $credentials_with_email = array('email' => request('email'), 'password'=>request('password'));
        $credentials_with_username = array('username' => request('email'), 'password'=>request('password'));

        if(!auth()->attempt($credentials_with_email,'false') && !auth()->attempt($credentials_with_username,'false')){
            return 'error';
        }

        $user = User::find(auth()->user()->id);
        $user->status = 'online';
        $user->loginTime = date('Y-m-d h:i:s');
        $user->save();

        return redirect('createjob');
    }

    public function registration(){
        $this->validate(request(),[
            'email'=>'required|email|unique:users',
            'password'=>'required|confirmed|min:6',
            'rules'=>'required'
        ]);
        $user = new User();
        $user->email = request('email');
        $user->password = \Hash::make(request('password'));
        $user->save();
        //login
        \Auth::login($user);
        $user->status = 'online';
        $user->loginTime = date('Y-m-d h:i:s');
        $user->save();

        return redirect('/');
    }

    public function create_job(){
        $job_types = Job_type::orderBy('order_id','asc')->get();
        $cities = City::get();
        $form_questions = Form_question::orderBy('order_id','asc')->get();
        return view('welcome.createjob',compact('job_types','cities','form_questions'))->with(['title'=>'Stazireba Contact Us','description'=>'Stazireba Contact Us']);
    }

    public function add_job(){
        $this->validate(request(),[
            'job_title' => 'required',
            'company_name' => 'required',
            'industry' => 'required',
            'salary_from' => 'required|numeric',
            'salary_to' => 'required|numeric',
            'requirement.*' => 'required',
            'job_type' => 'required|exists:job_types,id',
            'role_category' => 'required',
            'contact_email' => 'required|email',
            'contact_phone' => 'required',
            'address' => 'required',
            'city' => 'required|exists:cities,cityID'
        ]);

        $job = new Job;
        $job->title = request('job_title');
        $job->company_name = request('company_name');
        $job->industry = request('industry');
        $job->salary_from = request('salary_from');
        $job->salary_to = request('salary_to');
        $job->job_type_id = request('job_type');
        $job->role_category = request('role_category');
        $job->contact_email = request('contact_email');
        $job->contact_phone = request('contact_phone');
        $job->address = request('address');
        $job->countryID = 1;
        $job->cityID = request('city');
        $job->process_id = 3;
        $job->save();

        foreach(request('requirement') as $key => $requirement){
            $form_question = Form_question::where('request_name',$key)->first();
            $job_requirement = new Job_requirement();
            $job_requirement->job_id = $job->id;
            $job_requirement->requirement_title = $form_question->question;
            $job_requirement->requirement = $requirement;
            $job_requirement->save();
        }

        session(['info_message'=>'ვაკანსია წარმატებით დაემატა']);
        session(['info_message_text'=>$job->contact_email]);
        return redirect('infomessage');
    }

    public function edit_job($id){
        $job_types = Job_type::orderBy('order_id','asc')->get();
        $cities = City::get();
        $form_questions = Form_question::orderBy('order_id','asc')->get();
        $job = Job::find($id);
        return view('welcome.editjob',compact('job','job_types','cities','form_questions'))->with(['title'=>'Stazireba Contact Us','description'=>'Stazireba Contact Us']);
    }

    public function update_job(){
        $this->validate(request(),[
            'job_id'=>'required',
            'job_title' => 'required',
            'company_name' => 'required',
            'industry' => 'required',
            'salary_from' => 'required|numeric',
            'salary_to' => 'required|numeric',
            'requirement.*' => 'required',
            'job_type' => 'required|exists:job_types,id',
            'role_category' => 'required',
            'contact_email' => 'required|email',
            'contact_phone' => 'required',
            'address' => 'required',
            'city' => 'required|exists:cities,cityID',
            'process_id' => 'required|numeric'
        ]);

        $job = Job::find(request('job_id'));
        $job->title = request('job_title');
        $job->company_name = request('company_name');
        $job->industry = request('industry');
        $job->salary_from = request('salary_from');
        $job->salary_to = request('salary_to');
        $job->job_type_id = request('job_type');
        $job->role_category = request('role_category');
        $job->contact_email = request('contact_email');
        $job->contact_phone = request('contact_phone');
        $job->address = request('address');
        $job->countryID = 1;
        $job->cityID = request('city');
        $job->process_id = request('process_id');
        $job->save();

        foreach(request('requirement') as $key => $requirement){
            $form_question = Form_question::where('request_name',$key)->first();
            $job_requirement = Job_requirement::where('job_id',$job->id)->where('requirement_title',$form_question->question)->first();
            if(!is_null($job_requirement)) {
                $job_requirement->requirement = $requirement;
                $job_requirement->save();
            }
        }

        session(['info_message'=>'ვაკანსია წარმატებით განახლდა']);
        session(['info_message_text'=>$job->contact_email]);
        return redirect('infomessage');
    }

    public function jobs_by_status()
    {
        $this->validate(request(),[
            'process_id' => 'numeric|in:1,2,3'
        ]);

        $jobs = Job::where('process_id', request('process_id'))->orderBy('created_at', 'desc')->paginate(20);
        return view('welcome.jobsbystatus',compact('jobs'))->with(['title'=>'Stazireba jobs','description'=>'Stazireba show jobs']);
    }


    public function logout()
    {

        $user = User::find(auth()->user()->id);
        $user->status = 'offline';
        $user->logoutTime = date('Y-m-d h:i:s');
        $user->save();
        auth()->logout();
        session()->flush();
        return redirect('/');
    }
}
