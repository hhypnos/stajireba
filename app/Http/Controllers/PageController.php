<?php

namespace App\Http\Controllers;

use App\Brand;

use App\City;
use App\Form_question;
use App\Job;
use App\Job_requirement;
use App\Job_type;
use App\Job_view;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {
        if(session('languageID') === null){
            session(['languageID'=>1]);
        }
    }
    //start managment panel
    public function login()
    {
        return view('welcome.login',compact('brands'))->with(['title'=>'Stazireba','description'=>'Stazireba']);
    }
    //end: managment panel

    public function index()
    {
        $brands = Brand::orderBy('order_id','asc')->get();
        return view('welcome.index',compact('brands'))->with(['title'=>'Stazireba','description'=>'Stazireba']);
    }

    public function jobs()
    {
        $jobs = Job::where('process_id',3)->orderBy('created_at','desc')->paginate(20);
        return view('welcome.jobs',compact('jobs'))->with(['title'=>'Stazireba jobs','description'=>'Stazireba show jobs']);
    }

    public function job_details($id)
    {

        $job = Job::findOrFail($id);
        $job_view = new Job_view;
        $job_view->job_id = $id;
        if(\Auth::check()) {
            $job_view->user_id = auth()->user()->id;
         }
        $job_view->ip_address = request()->ip();
        $job_view->save();
        
        return view('welcome.jobdetails',compact('job'))->with(['title'=>$job->title,'description'=>$job->description]);
    }

    public function about_us()
    {
        return view('welcome.aboutus')->with(['title'=>'Stazireba Contact Us','description'=>'Stazireba Contact Us']);
    }

    public function contacts()
    {
        $job_types = Job_type::orderBy('order_id','asc')->get();
        $cities = City::get();
        $form_questions = Form_question::orderBy('order_id','asc')->get();
        return view('welcome.contact',compact('job_types','cities','form_questions'))->with(['title'=>'Stazireba Contact Us','description'=>'Stazireba Contact Us']);
    }

    public function send_form()
    {
        $this->validate(request(),[
            'job_title' => 'required',
            'company_name' => 'required',
            'industry' => 'required',
            'salary_from' => 'required|numeric',
            'salary_to' => 'required|numeric',
            'requirement.*' => 'required',
            'job_type' => 'required|exists:job_types,id',
            'role_category' => 'required',
            'contact_email' => 'required|email',
            'contact_phone' => 'required',
            'address' => 'required',
            'city' => 'required|exists:cities,cityID'
        ]);

        $job = new Job;
        $job->title = request('job_title');
        $job->company_name = request('company_name');
        $job->industry = request('industry');
        $job->salary_from = request('salary_from');
        $job->salary_to = request('salary_to');
        $job->job_type_id = request('job_type');
        $job->role_category = request('role_category');
        $job->contact_email = request('contact_email');
        $job->contact_phone = request('contact_phone');
        $job->address = request('address');
        $job->countryID = 1;
        $job->cityID = request('city');
        $job->save();

        foreach(request('requirement') as $key => $requirement){
            $form_question = Form_question::where('request_name',$key)->first();
            $job_requirement = new Job_requirement;
            $job_requirement->job_id = $job->id;
            $job_requirement->requirement_title = $form_question->question;
            $job_requirement->requirement = $requirement;
            $job_requirement->save();
        }

        session(['info_message'=>'ვაკანსია წარმატებით გაიგზავნა']);
        session(['info_message_text'=>$job->contact_email]);
        return redirect('infomessage');

    }

    public function info_message(){
        return view('welcome.infomessage')->with(['title'=>'Stazireba Contact Us','description'=>'Stazireba Contact Us']);
    }

    /**
     * @param $lang
     * @return translated text
     **/
    public function language($lang)
    {
        $language = Language::where('id',$lang)->first();
        if($language){
            session(['languageID' => $language->id]);
            return redirect(url()->to('/'));
        }
        return redirect('/')->withErrors(['error'=>translate('language not found',session('languageID'))]);
    }

    /**
     * @param $currencyID
     * @return currency of day
     */
    public function currency($currencyID)
    {
        $currency = Currency::where('id',$currencyID)->first();
        if($currency){
            session([
                "currency" => array(
                    "currency"=>$currency->currency,
                    "logo"=>$currency->logo,
                    "value"=>$currency->value
                )
            ]);
            return redirect(url()->to('/'));
        }
        return redirect('/')->withErrors(['error'=>translate('Currency not found',session('languageID'))]);
    }
//END: Currency

}
