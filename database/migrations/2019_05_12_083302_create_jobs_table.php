<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('company_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->float('salary_from')->nullable();
            $table->float('salary_to')->nullable();
            $table->integer('job_type_id')->nullable();
            $table->string('industry')->nullable();
            $table->string('role_category')->nullable();
            $table->integer('countryID')->nullable();
            $table->integer('cityID')->nullable();
            $table->string('zip_code',20)->nullable();
            $table->text('address')->nullable();
            $table->integer('process_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
