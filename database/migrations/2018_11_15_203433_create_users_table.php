<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',55)->nullable();
            $table->string('last_name',55)->nullable();
            $table->integer('gender_id')->nullable();
            $table->string('email',55)->unique();
            $table->string('password');
            $table->string('username',55)->unique()->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->enum('status',['offline','online']);
            $table->dateTime('loginTime')->nullable();
            $table->dateTime('logoutTime')->nullable();
            $table->integer('activation')->default('1');
            $table->integer('user_or_company');
            $table->timestamp('verifyied_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
